# Goals
- To understand how well the stream traces the progenitor's orbit
- Quantify the offset between the progentior's orbrit and the stream
- Understand which parameters are most important in determining the offset
- Aiming to correct these parameters to get a better fit if possible
- All of which is under the main goal bulding a Galactic potential model that incorporates streams

- (1) Look at _report-offset.ipynb_
    - here the goal was to look at the offset between the stream and the progenitor's orbit
    - additionally, we wanted to draw the mean path of the stream in galactic and galactocentric coordinates
- (2) Look at _report-predictors.ipynb_
    - here the goal was to see which properties could predict the offset between the stream and the orbit
    - perhaps the jacobii radius has the most predictive power
    - then, we looked to see if any orbital parameters could predict the offset. Doesn't appear so
- (3) Look at _report-inference.ipynb_
    - Lastly, we see if any morphology corresponds to features observable within the stream. We color the partciles based on their escape time, as well as some dynamical properties relating to their escape time. 


![Sample Video](videos/AM4.mp4)
