

# awk the strings from the first column of the file into a variable
# and then use that variable to move the files
# awk '{print $1}' file.txt | xargs -I {} mv {} /path/to/destination


awk -F "\"*,\"*" '{print $1}' ../../code/clean_stream.txt > cleanCGS.txt

mkdir -p trace-projection

while read -r line;
    do 
    cp ../../../plots/EDR3/PII/individual/$line/trace-projection.png ./trace-projection/$line.png;
done < cleanCGS.txt

mkdir -p yprime-xy
while read -r line;
    do 
    cp ../../../plots/EDR3/PII/individual/$line/yprime-xy.png ./yprime-xy/$line.png;
done < cleanCGS.txt

mkdir -p trace-deproject
while read -r line;
    do 
    cp ../../../plots/EDR3/PII/individual/$line/trace-deproject.png ./trace-deproject/$line.png;
done < cleanCGS.txt

mkdir yzprimeandhist
cp ../../../plots/EDR3/PII/individual/AM4/yzprimeandhist.png ./yzprimeandhist/AM4.png
cp ../../../plots/EDR3/PII/individual/Crater/yzprimeandhist.png ./yzprimeandhist/Crater.png
cp ../../../plots/EDR3/PII/individual/Pal5/yzprimeandhist.png ./yzprimeandhist/Pal5.png

