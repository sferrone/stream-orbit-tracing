"""
This set of functions allows the user to compute a stream using the particle spray
using the jacobi radius and using Galpy
"""
from galpy.orbit import Orbit
from galpy.potential import MWPotential2014 as MW14
from galpy.util import conversion
from galpy.potential import mass
import numpy as np 
import matplotlib.pyplot as plt 
import h5py 
import sys 
sys.path.append("/home/sferrone/GC-tidal-loss/simulations/analytic-GC-tidal-loss/code")
import inputMW
import inputGC
import os
import orbitCoords as OC
import astropy.coordinates as coord
import astropy.units as u
import time 
import multiprocessing as mp

def calcJacobii(laX,laY,laZ,satmass):
    """ CALCULATE THE THEORHETICAL JACOBI RADIUS from PII
    For a satelite orbiting the galactic center
    assuming, fasly, in a spherically symmetric potential
    SATMASS ALREADY IN SOLAR MASSES PLEASE
    
    """
    G=1
    TOSOLAR=2.32*10**7
    ax,ay,az,_=inputMW.potMW(laX,laY,laZ,0,"PII")
    nabPhi=np.sqrt(ax**2 + ay**2 + az**2)
    r = np.sqrt(laX**2 + laY**2 + laZ**2)
    MASS = nabPhi*(r**2)*G*TOSOLAR
    jacobii=2.88 * ((satmass/MASS)**(1/3))*r 
    return jacobii

def Jacobii(Pot,R,z,satmass,conv):
    """ CALCULATE THE THEORHETICAL JACOBI RADIUS
    SATMASS IN SOLAR MASSES PLEASE
    """
    massEnc=np.abs(mass(Pot,R,z)*conversion.mass_in_msol(conv['ro'],conv['vo']))
    r = np.sqrt(R**2 + z**2)
    rJAC=2.88*((satmass/massEnc)**(1/3))*r 
    return rJAC

def cyl_to_cart(cylin):
    """
    cylin=[R, vR, vT, z, vZ, phi]
    """
    R, vR, vT, z, vZ, phi=cylin[0],cylin[1],cylin[2],cylin[3],cylin[4],cylin[5]
    x,y=R*np.cos(phi),R*np.sin(phi)
    Rvec=np.array([x,y,0])
    Rmax=np.linalg.norm(Rvec)
    Runit=Rvec/Rmax 
    tanUnit=np.cross([0,0,1],Runit)
    VZ = np.array([0,0,vZ])
    VR = vR*Runit
    VT = vT*tanUnit
    vel=VZ+VR+VT
    return np.array([x,y,z,vel[0],vel[1],vel[2]])


def escapeInitialConditions(R, vR, vT, z, vZ, escapFunc, phi,satmass,conv):
    """
    SAT MASS ALREADY IN SOLAR 
    insert the coordinates of progenitor in natural units
    return the kinematics at the escape points in galpy format and in "natural" units
    assumes co-motion of the progenitor ie. forward moving orbit in time
    the interior and exterior escape particles have the same velocity as the GC
    escapFunc: a function used to calculate the escape radius. For now calcJacobii is supported
    """
    
    laX,laY=conv['ro']*R*np.cos(phi),conv['ro']*R*np.sin(phi)
    lat=np.arcsin(z/np.sqrt(R**2 + z**2))
    if escapFunc==calcJacobii:
        jacobii=escapFunc(laX,laY,conv['ro']*z,satmass)/conv['ro']
    if escapFunc==Jacobii:
        jacobii=Jacobii(MW14,R,z,satmass,conv)
    rint = np.sqrt(R**2 + z**2) - jacobii
    rext = np.sqrt(R**2 + z**2) + jacobii
    Rint,zint=rint*np.cos(lat),rint*np.sin(lat)
    Rext,zext=rext*np.cos(lat),rext*np.sin(lat)
    interior = [Rint,vR,vT,zint,vZ,phi]
    exterior = [Rext,vR,vT,zext,vZ,phi]
    return interior,exterior



def cart_to_cylin(COM):
    """
    position and velocities to cylindrical in the galpy calling sequence
    return these with units
    """ 
    vel=np.array([COM.v_x.value, COM.v_y.value, COM.v_z.value])
    RR = np.array([COM.x.value, COM.y.value,0])
    R=np.sqrt (COM.x**2 + COM.y**2)
    magR = np.linalg.norm(RR)*u.kpc
    RUnit = RR/np.linalg.norm(RR) # cannot use "Quantity" 
    zUnit=[0,0,1]
    tanUnit=np.cross(zUnit,RUnit)
    # project velocity onto these axis and re-add units so that they are "Qauntity" objs
    vT=np.dot(vel,tanUnit)*u.km/u.s
    vR=np.dot(vel,RUnit)*u.km/u.s
    vZ=np.dot(vel,[0,0,1])*u.km/u.s    
    phi = np.arctan2(COM.y,COM.x)
    phi=phi.value*(180/np.pi)*u.degree
    return R,vR,vT,COM.z,vZ,phi

def particleSpray(ts,fullorbit,satmass,conv,escapFunc):
    """ 
    All in natural units. 
    find the final positions of the particles that escape from the time stamps given by ts
    
    ts must be the N//2 where N is the length of full orbit

    escapFunc = a function that takes in the kinematics and and such and returns the kinematics of the particles at the escape points
    """
    N = ts.shape[0]
    leadingF=np.zeros((N-1,6))
    trailingF=np.zeros((N-1,6))
    tf=ts[-1]
    for i in range(N-1):
        R, vR, vT, z, vZ, phi = fullorbit[i,:]
        interior,exterior=escapeInitialConditions(R, vR, vT, z, vZ, escapFunc, phi,satmass,conv)
        intOrb=Orbit(interior)
        extOrb=Orbit(exterior)
        inttime=np.linspace(0,tf-ts[i],N-i)
        intOrb.integrate(inttime,MW14)
        extOrb.integrate(inttime,MW14)
        leadingF[i,:]=intOrb.getOrbit()[-1,:]
        trailingF[i,:]=extOrb.getOrbit()[-1,:]
    return leadingF, trailingF      


def makeoutpath(C):
    """
    Given the config with all of out information..
    """
    path="../../outputDATA/"
    os.makedirs(path,exist_ok=True)
    path+=C['dataset']+"/"
    os.makedirs(path,exist_ok=True)   
    path+=C["MWpoten"]+"/"
    os.makedirs(path,exist_ok=True)   
    path+=C["GCpoten"]+"/"
    os.makedirs(path,exist_ok=True)   
    path+="T-"+str(C['T'])+"/"
    os.makedirs(path,exist_ok=True)   
    path+="backward/"
    os.makedirs(path,exist_ok=True)   
    path+="streams/"
    os.makedirs(path,exist_ok=True)   
    path+="Np-"+str(C["Np"])+"/"
    os.makedirs(path,exist_ok=True)   
    return path


def writeoutH5(GCname,outpath,galacto,tEscOut):
    """ 
    Purpose:
    Given the stream in galactocentric coordinates, 
    write out everything!
    
    """
    hf = h5py.File(outpath+GCname+'.h5', 'w')
    sky = galacto.transform_to(coord.ICRS)
    ra = sky.ra.value
    dec = sky.dec.value
    D = sky.distance.value
    pm_ra_cosdec = sky.pm_ra_cosdec.value
    pm_dec = sky.pm_dec.value
    RV = sky.radial_velocity.value

    galactic = sky.transform_to('galactic')
    ll = galactic.l.value
    l = ll
    l[ll > 180] = l[ll > 180] - 360.
    b = galactic.b.value
    pm_l_cosb = galactic.pm_l_cosb.value
    pm_b = galactic.pm_b.value

    EQ = hf.create_group('equatorial')
    EQ.create_dataset('RA', data=ra)
    EQ.create_dataset('DEC', data=dec)
    EQ.create_dataset('D', data=D)
    EQ.create_dataset('PMRA_COSDEC', data=pm_ra_cosdec)
    EQ.create_dataset('PMDEC', data=pm_dec)
    EQ.create_dataset('RV', data=RV)
    GAL = hf.create_group('galactic')
    GAL.create_dataset('LONG', data=l)
    GAL.create_dataset('LAT', data=b)
    GAL.create_dataset('PML_COSB', data=pm_l_cosb)
    GAL.create_dataset('PMB', data=pm_b)
    GALCEN = hf.create_group('galactocentric')
    GALCEN.create_dataset('X', data=galacto.x.value)
    GALCEN.create_dataset('Y', data=galacto.y.value)
    GALCEN.create_dataset('Z', data=galacto.z.value)
    GALCEN.create_dataset('VX', data=galacto.v_x.value)
    GALCEN.create_dataset('VY', data=galacto.v_y.value)
    GALCEN.create_dataset('VZ', data=galacto.v_z.value)
    ENERGY = hf.create_group('energy')
    ENERGY.create_dataset('Tescape', data=tEscOut)
    hf.close()
    
    
    
def PSprayDT(pot,lorbit,satmass,conv,orbtime):
    """docstring for PSpray
    pot: potential
    lorbit : portion of an orbit moving forward in time with increase index
    satmass : the mass of the satellite in solar masses
    conv : dictionary of conversion factors
    orbtime : time array of the orbit starting at zero and moving forward in time
    """
    N = lorbit.shape[0]
    iOUT = np.zeros((N,6))
    eOUT = np.zeros((N,6))
    iINIT = np.zeros((N,6))
    eINIT = np.zeros((N,6))
    
    for i in range(N):
        R, vR, vT, z, vZ, phi = lorbit[i,:]
        interior,exterior=escapeInitialConditions(R, vR, vT, z, vZ, Jacobii,phi,satmass,conv)
        if i==N-1:
            iOUT[i,:]=interior
            iINIT[i,:]=interior
            eOUT[i,:]=exterior
            eINIT[i,:]=exterior
        else:
            ip=Orbit(interior)
            ep=Orbit(exterior)
            teval = np.array([orbtime.max()-orbtime[i], 0])
            ip.integrate(teval,pot,)
            ep.integrate(teval,pot,)
            iOUT[i,:] = ip.getOrbit()[-1,:]
            iINIT[i,:] = ip.getOrbit()[0,:]
            eOUT[i,:] = ep.getOrbit()[-1,:]
            eINIT[i,:] = ep.getOrbit()[0,:]
    return iOUT,eOUT,iINIT,eINIT,ip,ep




def PSprayT(pot,lorbit,satmass,conv,orbtime):
    """docstring for PSpray
    pot: potential
    lorbit : portion of an orbit moving forward in time with increase index
    satmass : the mass of the satellite in solar masses
    conv : dictionary of conversion factors
    orbtime : time array of the orbit starting at zero and moving forward in time
    """
    N = lorbit.shape[0]
    iOUT = np.zeros((N,6))
    eOUT = np.zeros((N,6))
    
    iINIT = np.zeros((N,6))
    eINIT = np.zeros((N,6))
    
    for i in range(N):
        R, vR, vT, z, vZ, phi = lorbit[i,:]
        interior,exterior=escapeInitialConditions(R, vR, vT, z, vZ, Jacobii,phi,satmass,conv)
        if i==N-1:
            iOUT[i,:]=interior
            iINIT[i,:]=interior
            eOUT[i,:]=exterior
            eINIT[i,:]=exterior
        else:
            ip=Orbit(interior)
            ep=Orbit(exterior)
            teval = np.array([orbtime.max(), orbtime[i]])
            ip.integrate(teval,pot,)
            ep.integrate(teval,pot,)
            iOUT[i,:] = ip.getOrbit()[-1,:]
            iINIT[i,:] = ip.getOrbit()[0,:]
            eOUT[i,:] = ep.getOrbit()[-1,:]
            eINIT[i,:] = ep.getOrbit()[0,:]
    return iOUT,eOUT,iINIT,eINIT,ip,ep


def main(GCname, tau=5, N=600):
    """ 
    do some particle spray!
    tau: integration time in 100 Myrs
    N: number of time stamps. Np=N-1

    """
    C={}
    C["dataset"]="EDR3"
    C["MWpoten"]="Bovy2014" # MWPotential2014
    C["GCpoten"]="PSpray"
    C["Nstep"] = N 
    C['deltat'] = None
    C['Np'] = N-1
    C["T"] = tau
    C["comment"] = "the integration time is in 100 Myrs"
    outpath=makeoutpath(C)
    
    # load in the data    
    COM=OC.getCOM(GCname)
    params=inputGC.Plummer(GCname)
    R,vR,vT,z,vZ,phi=cart_to_cylin(COM)
    
    # initalize galpy objects
    orbB=Orbit([R,-vR,-vT,z,-vZ,phi])
    orbF=Orbit([R,vR,vT,z,vZ,phi])
    conv=conversion.get_physical(orbB)
    # set up the integration time in natural units
    tauD = tau*(conv['vo']/conv['ro'])
    ts=np.linspace(0,tauD,N)
    # integrate total orbit
    orbB.integrate(ts,MW14)
    orbF.integrate(ts,MW14)
    # construct the full orbit
    borbit=orbB.getOrbit()
    forbit=orbF.getOrbit()
    borbit=np.flip(borbit,axis=0)
    borbit[:,[1,2,4]]*=-1
    fullorbit=np.concatenate([borbit[:-1,:],forbit])
    
    # perform the integration of all particle orbits
    stime=time.time()
    leadingF, trailingF=particleSpray(ts,fullorbit,params[0],conv,calcJacobii)
    print(GCname, time.time()-stime)
    # organize the data into an output
    leadCart=np.zeros(leadingF.shape)
    trailCart=np.zeros(trailingF.shape)
    for i in range(leadingF.shape[0]):
        leadCart[i,:] = cyl_to_cart(leadingF[i,:])
        trailCart[i,:] = cyl_to_cart(trailingF[i,:])
    x=np.concatenate((leadCart[:,0],trailCart[:,0]))*conv['ro']*u.kpc
    y=np.concatenate((leadCart[:,1],trailCart[:,1]))*conv['ro']*u.kpc
    z=np.concatenate((leadCart[:,2],trailCart[:,2]))*conv['ro']*u.kpc
    vx=np.concatenate((leadCart[:,3],trailCart[:,3]))*conv['vo']*u.km/u.s
    vy=np.concatenate((leadCart[:,4],trailCart[:,4]))*conv['vo']*u.km/u.s
    vz=np.concatenate((leadCart[:,5],trailCart[:,5]))*conv['vo']*u.km/u.s

    tsRealUnits = np.flip(ts[:-1]) * (conv['ro']/conv['vo'])
    tEscOut=np.concatenate((tsRealUnits,tsRealUnits))
    galacto=coord.SkyCoord(x=x,y=y,z=z,v_x=vx,v_y=vy,v_z=vz,frame=inputMW.ref_frame())
    
    writeoutH5(GCname,outpath,galacto,tEscOut)



# if __name__=="__main__":
#     tau=5
#     N=600
#     st=time.time()
#     GCnames,version,model=np.loadtxt("clean_stream.txt",delimiter=',',dtype=str,unpack=True)
#     pool = mp.Pool(mp.cpu_count())
#     for GCname in GCnames:
#         pool.apply_async(main,args=(GCname,tau,N))
#     pool.close()
#     pool.join()
#     pool.terminate()
# print("elapsed time:" , time.time()-st, "s")            
