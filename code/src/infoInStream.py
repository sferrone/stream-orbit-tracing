import numpy as np 
import matplotlib.pyplot as plt 
from mpl_toolkits.axes_grid1 import make_axes_locatable
import h5py 
import json
import sys 
import astropy.coordinates as coord
import astropy.units as u
import pandas as pd 
sys.path.append("/home/sferrone/GC-tidal-loss/simulations/analytic-GC-tidal-loss/code")
import inputMW
import inputGC
import orbitCoords as OC
import trace_orbit as TO 
import orbParams as OP





def scatterPlotWithColor(gcname,x,y,c1,c2,cond1,cond2,clabel,cmap="rainbow",xlabel="x' (kpc)",ylabel="y' (kpc)",):
    """
    Plot a scatter plot with colorbar

    Parameters
    ----------
    gcname : str
        name of the GC
    x : array
        x values
    y : array
        y values
    c : array
        color values
    cond1 : array   
        condition for the first scatter plot
    cond2 : array
        condition for the second scatter plot
    clabel : str
        label for the colorbar
    cmap : str
        colormap
    xlabel : str
        label for the x axis
    ylabel : str
        label for the y axis
    """
    fig,ax=plt.subplots(figsize=(15,5))
    ax.scatter(x[cond1],y[cond1],c=c1,s=1,cmap=cmap)
    im=ax.scatter(x[cond2],y[cond2],c=c2,s=1,cmap=cmap)
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="2%", pad=0.1)
    # add a colorbar that has the same size as the plot
    cbar = fig.colorbar(im, cax=cax,)
    ax.set_title(gcname)
    ax.set_xlabel(xlabel,fontsize=20)
    ax.set_ylabel(ylabel,fontsize=20)
    # make the colorbar flush with the plot
    cbar.ax.get_yaxis().labelpad = 15
    cbar.ax.set_ylabel(clabel, rotation=270,fontsize=20)
    # set equal axis
    ax.grid(True,linestyle='--',alpha=0.2)
    # ax.set_aspect('equal')
    fig.tight_layout()
    return fig,ax




def main():

    """
    do the y' plots with each particle colored by 
        (1) escape time
        (2) |w|
        (3) |dw/dt|
    """
    plt.style.use('dark_background')
    k=0
    ###
    GCnames,version,model=np.loadtxt("../little-data/clean_stream.txt",delimiter=',',dtype=str,unpack=True)
    f=open("../little-data/config.json")
    C = json.load(f)
    backwardpath,forwardpath=TO.orbitpath(C)
    streampath=TO.get_streampath(version=version[k],model=model[k])

    # time filter for the orbit in Gyrs
    dt = 0.3

    # iterate over each GC
    for k in range(7,len(GCnames)):
        print(GCnames[k])
        stream=h5py.File(streampath+GCnames[k]+".h5")
        COM = OC.getCOM(GCnames[k])
        t,W=TO.load_orbit(backwardpath,forwardpath,GCnames[k],C)

        # filter the orbit by time 
        cond = np.abs(t) < dt
        ts,Ws = t[cond],W[cond,:]
        past,future=(ts < 0),(ts > 0)
        x,y,z = Ws[:,0],Ws[:,1],Ws[:,2]
        vx,vy,vz = Ws[:,3],Ws[:,4],Ws[:,5]
        # store the center of mass
        xcom,ycom,zcom=COM.x.value,COM.y.value,COM.z.value
        # Get stream positions
        Xp,Yp,Zp=np.array(stream['galactocentric']['X']),np.array(stream['galactocentric']['Y']),np.array(stream['galactocentric']['Z'])
        VXp,VYp,VZp=np.array(stream['galactocentric']['VX']),np.array(stream['galactocentric']['VY']),np.array(stream['galactocentric']['VZ'])
        # initialize arrays for stream in orbit frame
        xprime,indexes,tt=np.zeros(Xp.shape),np.zeros(Xp.shape),np.zeros(Xp.shape)
        orbCoords,velCoords=np.zeros((Xp.shape[0],3)),np.zeros((Xp.shape[0],3))
        # initialize array for orbit in orbit frame
        xprimeORB=np.zeros(x.shape)
        tt=np.zeros(Xp.shape)
        # compute distaince along the orbit 
        xprimeORB=OC.define_orbital_coordinate(x, y, z, xcom, ycom, zcom, xprimeORB)
        tt,xprime,orbCoords,indexes,velCoords = OC.getorbcoords(Xp,Yp,Zp,VXp,VYp,VZp, ts,x,y,z,vx,vy,vz, xprimeORB,tt, xprime,orbCoords,velCoords,indexes)
        cond0 = stream['energy']['Tescape'][:]>0
        cond1,cond2  = xprime < 0,xprime > 0
        cond1 = np.logical_and(cond0,cond1)
        cond2 = np.logical_and(cond0,cond2)

        # compute the omaga and omega dot at each time 
        omega,omegamag,omegaDotMag=OP.getOmegaDot(t,W)
        # find the closest time to the stream particle
        mytimes1=stream['energy']['Tescape'][cond1]/10 - 5
        mytimes2=stream['energy']['Tescape'][cond2]/10 - 5
        tindex1 = np.array([np.argmin(np.abs(t-ti)) for ti in mytimes1])
        tindex2 = np.array([np.argmin(np.abs(t-ti)) for ti in mytimes2])
        # get the omega and omega dot at the closest time
        omegamag1,omegamag2=omegamag[tindex1],omegamag[tindex2]
        omegaDotMag1,omegaDotMag2=omegaDotMag[tindex1],omegaDotMag[tindex2]

        # plot the scatter plot with colorbar
        basepath="../../../plots/"+version[k]+"/"+model[k]+"/individual/"+GCnames[k]+"/"
        fig,_=scatterPlotWithColor(GCnames[k],xprime,orbCoords[:,0],mytimes1,mytimes2,cond1,cond2,clabel='T$_{esc}$ (Gyrs ago)')
        fig.savefig(basepath+"/yprime_tescape.png",dpi=300)
        plt.close(fig)
        fig,_=scatterPlotWithColor(GCnames[k],xprime,orbCoords[:,0],omegamag1,omegamag2,cond1,cond2,clabel='$|\omega|$@ escape')
        fig.savefig(basepath+"/yprime_omega.png",dpi=300)
        plt.close(fig)
        fig,_=scatterPlotWithColor(GCnames[k],xprime,orbCoords[:,0],np.abs(omegaDotMag1),np.abs(omegaDotMag2),cond1,cond2,clabel='|$\dot{\omega}|$@ escape')
        fig.savefig(basepath+"/yprime_omegadot.png",dpi=300)
        plt.close(fig)

if __name__ == "__main__":
    main()