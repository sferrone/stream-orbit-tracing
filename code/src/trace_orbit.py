import orbitCoords
import matplotlib.pyplot as plt 
import numpy as np 
import h5py
import json
from scipy.io import FortranFile
import sys
import time
import multiprocessing as mp
import os 

def orbitpath(C,root="/home/sferrone/outputDATA/"):
    backwardpath=root+C['DataSet']+"/"+C["MWpoten"]+"/"+C["GCpoten"]+"/dt-"+str(C['deltat'])+"/backward/Nstep-"+str(C['Nstep'])+"/GCorbits"
    forwardpath=root+C['DataSet']+"/"+C["MWpoten"]+"/"+C["GCpoten"]+"/dt-"+str(C['deltat'])+"/forward/Nstep-"+str(C['Nstep'])+"/GCorbits"
    return backwardpath,forwardpath



def extract_orbit(path,GCname):
    ff=FortranFile(path+"/orbit"+GCname+".bin")
    lashape=ff.read_ints()
    W = np.zeros(lashape)
    for i in range(lashape[0]):
        W[i,:]=ff.read_reals()
    return W


def load_orbit(backwardpath,forwardpath,GCname,C):
    Wb = extract_orbit(backwardpath,GCname)
    Wf = extract_orbit(forwardpath, GCname)
    dt = C['deltat']*10**-9
    T = dt*C['Nstep']
    tb=np.arange(0,-T-dt,-dt)
    tf=np.arange(0,T+dt,dt)
    # delete duplicate of origin
    Wf=np.delete(Wf,0,0)
    tf=np.delete(tf,0)
    tb=np.flipud(tb)
    Wb=np.flipud(Wb)
    # make backward velocity positive
    Wb[0:-1,3:6]*=-1
    W=np.concatenate((Wb,Wf))
    t=np.concatenate((tb,tf))    
    return t,W


def get_streampath(version="EDR3",model="PII"):
    """ 
    model = 'PI' or 'PII' or 'PII_0.3_SLOW'
    version = 'EDR3' OR 'EDR3_NEW'
    'EDR3' for large time stamp.
    'EDR3_NEW' for small time stamp
    """
    print(version,model)
    path="/mod4gaia/GCsTT/"+version+"/outputDATA/"+model+"/streams/"
    return path
    
    
def load_orbit_stream_COM(GCname, version="EDR3", model="PII"):
    f=open("../little-data/config.json")
    C = json.load(f)
    # load stream
    streampath=get_streampath(version=version,model=model)  
    stream=h5py.File(streampath+GCname+".h5")
    Xp,Yp,Zp=np.array(stream['galactocentric']['X']),np.array(stream['galactocentric']['Y']),np.array(stream['galactocentric']['Z'])
    # load COM
    COM = orbitCoords.getCOM(GCname)
    backwardpath,forwardpath=orbitpath(C,root="/home/sferrone/outputDATA/")
    t,W=load_orbit(backwardpath,forwardpath,GCname,C)
    return t,W,Xp,Yp,Zp,COM,stream
    
def absTimeFilter(dt,t,W):
    """
    take the kinematics and filter based on how far into the past and future we want to go
    """
    cond = np.abs(t) < dt
    ts,Ws = t[cond],W[cond,:]
    past,future=(ts < 0),(ts > 0)
    x,y,z = Ws[:,0],Ws[:,1],Ws[:,2]
    vx,vy,vz = Ws[:,3],Ws[:,4],Ws[:,5]    
    return past,future,x,y,z,vx,vy,vz


def plot_prime_and_galcentric(xprime,orbCoords,past,future,x,y,z,Xp,Yp,Zp,cond0,myalpha):
    """
    purpose: show x' versus y'
    """
    MEDIUM_SIZE=15
    plt.style.use('dark_background')
    plt.rc('axes', labelsize=MEDIUM_SIZE) 
    fig,ax=plt.subplots(1,2,figsize=(10,4))
    cond1=xprime < 0
    cond1=np.logical_and(cond1,cond0)
    cond2=xprime > 0
    cond2=np.logical_and(cond2,cond0)
    
    ax[0].plot(xprime,orbCoords[:,0].mean()*np.ones(xprime.shape),c='w',zorder=0,linewidth=0.5)
    ax[0].plot(xprime[cond1],orbCoords[cond1,0].mean()*np.ones(xprime[cond1].shape),c='w',zorder=0,linewidth=0.5)
    ax[0].plot(xprime[cond2],orbCoords[cond2,0].mean()*np.ones(xprime[cond2].shape),c='w',zorder=0,linewidth=0.5)
    ax[0].scatter(xprime,orbCoords[:,0], alpha=myalpha,s=1)
    ax[0].set_xlabel("x' (kpc)")
    ax[0].set_ylabel("y' (kpc)")
    ax[0].set_title("Along orbit coords")
    
    ax[1].plot(y[past],z[past], 'r',zorder=0,linewidth=0.5)
    ax[1].plot(y[future],z[future], 'g',zorder=0,linewidth=0.5)
    ax[1].scatter(Yp,Zp, alpha=myalpha,s=1)    
    
    ax[1].set_xlabel("Y (kpc)")
    ax[1].set_ylabel("Z (kpc)")
    ax[1].set_title("Galactocentric")
    
    ymin,ymax=ax[1].get_ylim()
    xmin,xmax=ax[1].get_xlim()
    X0=0.02*(xmax-xmin)+xmin
    Y0=0.95*(ymax-ymin)+ymin
    ax[1].text(X0,Y0,"LEADING",c="g")
    
    dy1=orbCoords[cond1,0].mean()
    dy2=orbCoords[cond2,0].mean()
    ymin,ymax=ax[0].get_ylim()
    xmin,xmax=ax[0].get_xlim()
    ax[0].text(xmax/10,ymin/2,r"$\delta$ y' = {:.1f} pc".format(1000*dy2))
    ax[0].text(2*xmin/3,ymin/2,r"$\delta$ y' = {:.1f} pc".format(1000*dy1))
    # fig.suptitle(GCname)
    # fig.tight_layout()
    
    # outname = GCname+"xyprime_YZ.png"
    # fig.savefig(outpath+outname,dpi=300)
    # plt.close(fig)
    return fig,ax
    
    

    
def plot_xyprirme(GCname, dt, version, model="PII"):
    """
    PURPOSE:
        GET GC AND PLOT IT'S x' and y'
    """
    t,W,Xp,Yp,Zp,COM,stream=load_orbit_stream_COM(GCname,version=version,model=model)
    past,future,x,y,z,vx,vy,vz = absTimeFilter(dt,t,W)
    VXp,VYp,VZp=stream['galactocentric']['VX'][:],stream['galactocentric']['VY'][:],stream['galactocentric']['VZ'][:]
    # initialize output
    xprime,indexes,tt=np.zeros(Xp.shape),np.zeros(Xp.shape),np.zeros(Xp.shape)
    orbCoords,velCoords=np.zeros((Xp.shape[0],3)),np.zeros((Xp.shape[0],3))
    xprimeORB=np.zeros(x.shape)
    xcom,ycom,zcom=COM.x.value,COM.y.value,COM.z.value
    xprimeORB=orbitCoords.define_orbital_coordinate(x, y, z, xcom, ycom, zcom, xprimeORB)
    tt, xprime, orbCoords, indexes,velCoords = orbitCoords.getorbcoords(
    Xp, Yp, Zp, VXp, VYp, VZp, t, x, y, z, vx, vy, vz, xprimeORB, tt, xprime, orbCoords, velCoords, indexes)
    
    cond0=stream['energy']['Tescape'][:]>0
    myalpha=1-np.sum(stream['energy']['Tescape'][:] > 0) / stream['energy']['Tescape'][:].shape[0]
    fig,ax=plot_prime_and_galcentric(xprime,orbCoords,past,future,x,y,z,Xp,Yp,Zp,cond0,myalpha=myalpha)
    fig.suptitle(GCname)
    fig.tight_layout()
    outdir="../../plots/"+version+"/"+model+"/individual/"+GCname+"/"
    print(outdir+"yprime-xy.png")
    fig.savefig(outdir+"yprime-xy.png",dpi=200)
    plt.close(fig)
    
    
if __name__=="__main__":
    """
    PURPOSE:
        GET GC AND PLOT IT'S x' and y'    
        INPUT:
            GCname: name of GC
            dt: time interval in Gyr
            version: version of the data
            model: model of the data
        OUTPUT:
            plot of x' and y' of GC
                
    """
    st = time.time()
    GClg=np.loadtxt("GClargeDT.txt",dtype=str)
    GCsm=np.loadtxt("GCsmallDT.txt",dtype=str)
    dt=0.2
    pool=mp.Pool(mp.cpu_count())
    print("# CPUs:",mp.cpu_count())
    model="PII"
    os.makedirs("../../plots/",exist_ok=True)
    version="EDR3_new"
    os.makedirs("../../plots/"+version,exist_ok=True)
    os.makedirs("../../plots/"+version+"/"+model,exist_ok=True)
    os.makedirs("../../plots/"+version+"/"+model+"/individual/",exist_ok=True)
    print("OK")
    print("../../plots/"+version+"/"+model+"/individual/")

    for i in range(len(GCsm)):
        outdir="../../plots/"+version+"/"+model+"/individual/"+GCsm[i]+"/"
        os.makedirs(outdir,exist_ok=True)
        print(outdir)
        pool.apply_async(plot_xyprirme,args=(GCsm[i],dt,version,model))
        
    version="EDR3"
    os.makedirs("../../plots/"+version,exist_ok=True)
    os.makedirs("../../plots/"+version+"/"+model,exist_ok=True)
    os.makedirs("../../plots/"+version+"/"+model+"/individual/",exist_ok=True)
    print("OK")
    print("../../plots/"+version+"/"+model+"/individual/")
    for i in range(len(GClg)):
        outdir="../../plots/"+version+"/"+model+"/individual/"+GClg[i]+"/"
        print(outdir)
        os.makedirs(outdir,exist_ok=True)
        pool.apply_async(plot_xyprirme,args=(GClg[i],dt,version,model))
    
    
    pool.close()
    pool.join()
    pool.terminate()    
    
    print("elapsed time:" , time.time()-st, "s")