import orbitCoords as OC
import trace_orbit as TO
import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import CubicSpline



def getOmega(GCnames):
    """
    load in the orbital parameters of each GC's COM
    RETURNS:
        omega: the angular velocity of the GC's COM
        omegaNorm: the mangitude of the angular velocity vector
    """
    omegas,omegaNorm=[],[]
    for k in range(len(GCnames)):
        COM=OC.getCOM(GCnames[k])
        rvec=[COM.x.value,COM.y.value,COM.z.value]
        vvec=[COM.v_x.value,COM.v_y.value,COM.v_z.value]
        omega=np.cross(rvec,vvec)/np.linalg.norm(rvec)**2
        omegas.append(omega)
        omegaNorm.append(np.linalg.norm(omega))
    return omegas,omegaNorm



def ColorBarSampling(GCnames,CMAP="hsv"):
    """
    plot the yprime values for each GC
    """
    cmap = plt.cm.get_cmap(CMAP)
    GCnames=np.array(GCnames)
    mycolors=[]
    sampling=np.linspace(0,1,len(GCnames))
    for i in range(len(GCnames)):
        mycolors.append(cmap(sampling[i]))
    return mycolors

def RandomMirrorLabelling(GCnames):
    """
    Determine if the label of the GC will be applied to the top or bottom of the stream
    """
    topBottom=np.random.rand(len(GCnames))
    condB=topBottom<0.5
    condT=topBottom>0.5
    topBottom[condB]=1
    topBottom[condT]=0
    return topBottom

def yprimePlot(x,yprimes,GCnames,scatterColors,topBottom,xlabel=r"$\omega$ (km s$^{-1}$ kpc$^{-1}$)",ylabel=r"$\bar{\delta y'}$ (kpc)"):
    """
    plot the yprime values for each GC
    """
    fig,ax=plt.subplots(figsize=(8,6))
    ax.scatter(x,yprimes['trailing'],c=scatterColors)
    ax.scatter(x,yprimes['leading'],c=scatterColors)
    ax.grid(True,alpha=0.1)
    for k in range(len(GCnames)):
        if topBottom[k]:
            ax.text(x[k],yprimes['leading'][k],GCnames[k])
        else:
            ax.text(x[k],yprimes['trailing'][k],GCnames[k])
    ax.set_ylabel(ylabel,size=15);
    ax.set_xlabel(xlabel,size=15);
    return fig,ax 


# Get the cubic spline of omega to obtain omega dot
def getOmegaDot(t,W):
    """
    get the derivative of omega
    """
    pos=W[:,:3]
    vel=10*W[:,3:] # remember the integration units are 10 km/s
    RxV=np.cross(pos,vel,axis=1)
    R2 = np.linalg.norm(pos,axis=1)**2
    omega = np.array([RxV[i]/R2[i] for i in range(len(R2))])
    omegamag=np.linalg.norm(omega,axis=1)
    OmegaDotFunc=CubicSpline(t,omegamag,1)
    omegaDot=OmegaDotFunc(t,1)
    return omega,omegamag,omegaDot
    

