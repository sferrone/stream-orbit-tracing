import matplotlib.pyplot as plt 
import numpy as np 
import h5py
import json 
import sys 
import os 
from scipy.io import FortranFile
from astropy.io import fits
import astropy.coordinates as coord
import astropy.units as u
sys.path.append("/home/sferrone/GC-tidal-loss/experiments/functions/")
import inputMW
import global_functions as GF
import orbitCoords as OC
import trace_orbit as TO 


def deproject(xprimeORB,indexes,ids,cond0,x,y,z,vx,vy,vz,xCen,yMeans,zMeans):
    """
    deproject stream particles back into galactocentric coordinates

    xprimeORB = distance from GC's COM along the orbit
    indexes = indexes of stream particles
    ids = ids of stream particles
    cond0 = conditional array indicate the particles that have escaped
    x,y,z,vx,vy,vz = coordiantes of the orbit 
    xCen,yMeans,zMeans = the mean-binned stream off set in along-orbit coordinates

    returns
    xX,yY,zZ are the coordinates are the orbit. only those that were used
    XpDE,YpDE,ZpDE = coordinates of stream that are deprojected back into galactocentric
    """
    idxPartToOrb=np.array(indexes[cond0][ids],dtype=int)
    idxPartToOrb=np.unique(idxPartToOrb)
    xX=x[idxPartToOrb]
    yY=y[idxPartToOrb]
    zZ=z[idxPartToOrb]
    XpDE,YpDE,ZpDE=np.zeros(xCen.shape),np.zeros(xCen.shape),np.zeros(xCen.shape)
    for i in range(xCen.shape[0]):
        deID=np.argmin(np.abs(xCen[i]-xprimeORB))
        # make local x and v
        r=np.array([x[deID],y[deID],z[deID]])
        v=np.array([vx[deID],vy[deID],vz[deID]])
        runit=r/np.linalg.norm(r)
        vunit=v/np.linalg.norm(v)
        zunit=np.cross(runit,vunit)
        P=yMeans[i]*runit + zMeans[i]*zunit + (xCen[i]-xprimeORB[deID])*vunit
        XpDE[i],YpDE[i],ZpDE[i]=P[0]+x[deID],P[1]+y[deID],P[2]+z[deID]
    return XpDE,YpDE,ZpDE,xX,yY,zZ

def yzprimeandhist(xprimeS,yprime,zprime,xCen,yMeans,zMeans,nInBin,nBins):
    fig,ax=plt.subplots(3,1,sharex=True)
    ax[0].hist(xprimeS,bins=nBins,facecolor='r',alpha=0.4);
    ax[0].set_ylabel('N (E>0)')
    ax[1].scatter(xprimeS,zprime,alpha=0.1)
    ax[1].scatter(xCen,zMeans,c='r',s=nInBin/5,alpha=0.1)
    ax[1].plot(xCen,zMeans,c='r')
    ax[1].set_ylabel("z' (pc)")

    ax[2].scatter(xprimeS,yprime,alpha=0.1)
    ax[2].scatter(xCen,yMeans,c='r',s=nInBin/5,alpha=0.1)
    ax[2].plot(xCen,yMeans,c='r')
    ax[2].set_ylabel("y' (pc)")
    ax[2].set_xlabel("x' (pc)")
    return fig,ax

# only take the ones that are outside!!
def meanYZprimes(xprime,orbCoords,cond0,binPOW=1/3):
    """
    Bin along the xprime axis according to binPOW
    return the mean y and z prime values for each bin
    xprime = distance from GC's COM along the orbit
    orbCoords = the stream offset in along-orbit coordinates
    cond0 = conditional array indicate the particles that have escaped
    binPOW = the power to which the number of particles is raised to determine the number of bins
    RETURNS:
    xCen = the center of the bins
    yMeans,zMeans = the mean-binned stream off set in along-orbit coordinates
    nInBin = the number of particles in each bin
    xprimeS,yprime,zprime = the stream offset in along-orbit coordinates
    ids = the ids of the particles in the stream
    """
    xprimeS=xprime[cond0]
    yprime=orbCoords[cond0,0]
    zprime=orbCoords[cond0,1]
    ids=np.argsort(xprimeS)
    xprimeS=xprimeS[ids]
    yprime=yprime[ids]
    zprime=zprime[ids]
    nBins=int(np.floor(np.power(xprimeS.shape[0],binPOW)))
    xWall=np.linspace(xprimeS.min(),xprimeS.max(),nBins)
    xCen=np.array( [(xWall[i+1] +xWall[i])/2 for i in range(nBins-1) ])
    yMeans,zMeans,nInBin = np.zeros(nBins-1),np.zeros(nBins-1),np.zeros(nBins-1)
    for i in range(nBins-1):
        condBtwn=np.logical_and( xWall[i] < xprimeS, xprimeS < xWall[i+1]  )
        yMeans[i]=np.mean(yprime[condBtwn])
        zMeans[i]=np.mean(zprime[condBtwn])
        nInBin[i]=np.sum(condBtwn)
    return xCen,yMeans,zMeans,nInBin,xprimeS,yprime,zprime,ids,nBins


def deprojectplot(xX,yY,zZ,XpDE,YpDE,ZpDE):
    """ tbh might not need a function for this """
    fig,ax=plt.subplots(1,2,sharey=True,figsize=(10,4))
    ax[0].plot(xX,yY)
    ax[0].plot(XpDE,YpDE)

    ax[1].plot(zZ,yY)
    ax[1].plot(ZpDE,YpDE)
    ax[0].set_ylabel("Y (kpc)")
    ax[0].set_xlabel("X (kpc)")
    ax[1].set_xlabel("Z (kpc)")
    return fig,ax

def main():
    """
    PURPOSE
    study the track one could find from the stream per GC
    """
    plt.style.use('dark_background')
    GCnames,version,model=np.loadtxt("clean_stream.txt",delimiter=',',dtype=str,unpack=True)
    # load config
    f=open("config.json")
    C = json.load(f)
    dt=0.3
    backwardpath,forwardpath=TO.orbitpath(C)
    os.makedirs("/home/sferrone/plots/",exist_ok=True)

    
    
    
    for k in range(len(GCnames)):
        os.makedirs("/home/sferrone/plots/version/",exist_ok=True)
        os.makedirs("/home/sferrone/plots/EDR3/PII/",exist_ok=True)
        os.makedirs("/home/sferrone/plots/EDR3/PII/individual/",exist_ok=True)

        os.makedirs("/home/sferrone/plots/"+version[k]+"/",exist_ok=True)
        os.makedirs("/home/sferrone/plots/"+version[k]+"/"+model[k]+"/",exist_ok=True)
        outpath="/home/sferrone/plots/"+version[k]+"/"+model[k]+"/individual/"
        os.makedirs(outpath,exist_ok=True)
        outpath="/home/sferrone/plots/"+version[k]+"/"+model[k]+"/individual/"+GCnames[k] +"/"
        os.makedirs(outpath,exist_ok=True)
        streampath=TO.get_streampath(version=version[k],model=model[k])
        stream=h5py.File(streampath+GCnames[k]+".h5")
        COM = OC.getCOM(GCnames[k])
        
        t,W,Xp,Yp,Zp,COM,stream=TO.load_orbit_stream_COM(GCnames[k],version=version[k],model=model[k])
        past,future,x,y,z,vx,vy,vz = TO.absTimeFilter(dt,t,W)
        VXp,VYp,VZp=stream['galactocentric']['VX'][:],stream['galactocentric']['VY'][:],stream['galactocentric']['VZ'][:]
        # initialize output
        xprime,indexes,tt=np.zeros(Xp.shape),np.zeros(Xp.shape),np.zeros(Xp.shape)
        orbCoords,velCoords=np.zeros((Xp.shape[0],3)),np.zeros((Xp.shape[0],3))
        xprimeORB=np.zeros(x.shape)
        xcom,ycom,zcom=COM.x.value,COM.y.value,COM.z.value
        xprimeORB=OC.define_orbital_coordinate(x, y, z, xcom, ycom, zcom, xprimeORB)
        tt, xprime, orbCoords, indexes,velCoords = OC.getorbcoords(
        Xp, Yp, Zp, VXp, VYp, VZp, t, x, y, z, vx, vy, vz, xprimeORB, tt, xprime, orbCoords, velCoords, indexes)
        cond0=stream['energy']['Tescape'][:]>0
        
        xCen,yMeans,zMeans,nInBin,xprimeS,yprime,zprime,ids,nBins = meanYZprimes(xprime,orbCoords,cond0,binPOW=1/3)
        fig,ax=yzprimeandhist(xprimeS,yprime,zprime,xCen,yMeans,zMeans,nInBin,nBins)
        outname="yzprimeandhist.png"
        fig.suptitle(GCnames[k])
        fig.tight_layout()
        fig.savefig(outpath+outname)
        plt.close(fig)
        
        
        # do the de projection
        XpDE,YpDE,ZpDE,xX,yY,zZ=deproject(xprimeORB,indexes,ids,cond0,x,y,z,vx,vy,vz,xCen,yMeans,zMeans)
        fig,ax=deprojectplot(xX,yY,zZ,XpDE,YpDE,ZpDE)
        fig.suptitle(GCnames[k]);
        outname="trace-deproject.png"
        fig.suptitle(GCnames[k])
        fig.tight_layout()
        fig.savefig(outpath+outname)
        plt.close(fig)        
        
        
        # projection onto the sky
        c=coord.SkyCoord(x=xX*u.kpc,y=yY*u.kpc,z=zZ*u.kpc,frame=inputMW.ref_frame())
        cORB=c.transform_to(coord.Galactic())
        c=coord.SkyCoord(x=XpDE*u.kpc,y=YpDE*u.kpc,z=ZpDE*u.kpc,frame=inputMW.ref_frame())
        cSTM=c.transform_to(coord.Galactic())
        fig,ax=plt.subplots(1,1)
        ax.plot(cORB.l.wrap_at(180*u.degree),cORB.b),ax.plot(cSTM.l.wrap_at(180*u.degree),cSTM.b)
        ax.set_aspect("equal"),ax.set_ylabel("lat (deg)"),ax.set_xlabel("long (deg)"),ax.set_title(GCnames[k])        
        outname="trace-projection.png"
        fig.tight_layout()
        fig.savefig(outpath+outname)
        plt.close(fig)     
        
        
        # do cummulative
        yMeans[np.isnan(yMeans)]=0#np.mean(yMeans[~np.isnan(yMeans)])
        fig,ax=plt.subplots(1,1)
        ax.plot(xCen,np.cumsum(yMeans))
        ax.set_xlabel("x' (kpc)"),ax.set_ylabel("CUMSUM(y') (kpc)"),ax.grid('on',alpha=0.2),ax.set_title(GCnames[k])           
        outname="stream-cumsum.png"
        fig.tight_layout()
        fig.savefig(outpath+outname)
        plt.close(fig)   
          
                
        
        
        
        
if __name__=="__main__":
    main()