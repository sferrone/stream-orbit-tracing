import matplotlib.pyplot as plt 
import numpy as np 
import h5py
import json 
import sys 
sys.path.append("/home/sferrone/GC-tidal-loss/experiments/functions/")
import orbitCoords as OC
import trace_orbit as TO 
from scipy.interpolate import CubicSpline
import multiprocessing as mp
import os
import time

def initializeAndObtainOrbCoords(stream,COM,ts,x,y,z,vx,vy,vz):
    """
    PURPOSE!
    Initialize the orbit coordaintes and grab them.

    stream: h5 file of the stream
    COM: CenterOfMass of the GC today stored in an astropy object
    t,x,y,z,vx,vy,vz : the backward and forward concatenated orbit
    """
    Xp,Yp,Zp=np.array(stream['galactocentric']['X']),np.array(stream['galactocentric']['Y']),np.array(stream['galactocentric']['Z'])
    VXp,VYp,VZp=stream['galactocentric']['VX'][:],stream['galactocentric']['VY'][:],stream['galactocentric']['VZ'][:]
    xcom,ycom,zcom=COM.x.value,COM.y.value,COM.z.value
    xprime,indexes,tt=np.zeros(Xp.shape),np.zeros(Xp.shape),np.zeros(Xp.shape)
    orbCoords,velCoords=np.zeros((Xp.shape[0],3)),np.zeros((Xp.shape[0],3))
    xprimeORB=np.zeros(x.shape)
    xprimeORB=OC.define_orbital_coordinate(x, y, z, xcom, ycom, zcom, xprimeORB)
    tt, xprime, orbCoords, indexes,velCoords = OC.getorbcoords(
        Xp, Yp, Zp, VXp, VYp, VZp, ts, x, y, z, vx, vy, vz, xprimeORB, tt, xprime, orbCoords, velCoords, indexes)
    cond0 = stream['energy']['Tescape'][:]>0
    cond1,cond2 = xprime < 0, xprime > 0
    cond1,cond2 = np.logical_and(cond0,cond1),np.logical_and(cond0,cond2)    
    return xprimeORB, tt, xprime, orbCoords, indexes, cond1,cond2

def main(GCname,VER,MODEL):
    """ 
    PURPOSE:
    Make a composite plot of the orbital coordinate
    y' vs time of escape
    omega' vs time of escape
    number of particles escaping vs time of escape 
    OUT
        images saved as omegaDotYPrimeAndTesc
    """
    plt.style.use('dark_background')
    COM = OC.getCOM(GCname)
    f=open("config.json")
    C = json.load(f)
    dt=0.3
    backwardpath,forwardpath=TO.orbitpath(C)
    streampath=TO.get_streampath(version=VER,model=MODEL)
    t,W=TO.load_orbit(backwardpath,forwardpath,GCname,C)
    stream=h5py.File(streampath+GCname+".h5")
    cond = np.abs(t) < dt
    ts,Ws = t[cond],W[cond,:]
    past,future=(ts < 0),(ts > 0)
    x,y,z = Ws[:,0],Ws[:,1],Ws[:,2]
    vx,vy,vz = Ws[:,3],Ws[:,4],Ws[:,5]


    xprimeORB, tt, xprime, orbCoords, indexes, cond1,cond2=\
        initializeAndObtainOrbCoords(stream,COM,ts,x,y,z,vx,vy,vz)
    # zero time to today
    cond=stream['energy']['Tescape'][:] > 0
    tesc=stream['energy']['Tescape'][cond]/10 - 5 
    x,y,z = W[:,0],W[:,1],W[:,2]
    vx,vy,vz = W[:,3],W[:,4],W[:,5]

    vel=np.zeros((vx.shape[0],3))
    pos=np.zeros((vx.shape[0],3))
    pos[:,0],pos[:,1],pos[:,2] = x,y,z
    vel[:,0],vel[:,1],vel[:,2] = vx,vy,vz
    # calculate omega
    omega=np.cross(pos,vel,axis=1)/(np.linalg.norm(pos)**2)
    omegamag=np.linalg.norm(omega,axis=1)
    ospline=CubicSpline(t,omegamag)
    nbins=int(np.ceil(np.sqrt(tesc.shape[0])))


    fig,ax=plt.subplots(3,1,figsize=(7,5),sharex=True)
    ax[1].scatter(tesc,orbCoords[cond,0], s=1,alpha=0.1)
    ax[0].plot(t,ospline(t,1))
    ax[0].set_ylabel(r"$\dot{\omega} (km s^{-1}kpc^{-1}Gyrs^{-1})$")

    ax[2].set_xlabel(r"$t_{esc}$ (Gyrs)",size=15);
    ax[2].hist(tesc,bins=nbins);

    ax[0].set_xticks(np.linspace(-5,0,11));
    ax[1].set_xticks(ax[0].get_xticks());
    ax[2].set_xticks(ax[0].get_xticks());

    ax[1].set_ylabel(r"$\delta$ y' (kpc)");
    ax[1].set_title("at today",size=7)

    ax[2].set_ylabel("# esc")
    for xxx in ax:
        xxx.grid(True,alpha=0.3)
        xxx.set_xlim(-5,0)

    fig.tight_layout()
    fig.suptitle(GCname)
    outpath="/home/sferrone/plots/"+VER+"/"+MODEL+"/individual/"+GCname+"/"
    fig.savefig(outpath+"omegaDotYPrimeAndTesc.png")
    plt.close(fig)
    print("DONE WITH ", GCname)



    

if __name__=="__main__":
    GCnames,version,model=np.loadtxt("clean_stream.txt",delimiter=',',dtype=str,unpack=True)

    pool = mp.Pool(mp.cpu_count())
    os.makedirs("/home/sferrone/plots/",exist_ok=True)
    st=time.time()
    for k in range(len(GCnames)):
        os.makedirs("/home/sferrone/plots/"+version[k]+"/",exist_ok=True)
        os.makedirs("/home/sferrone/plots/"+version[k]+"/"+model[k]+"/",exist_ok=True)
        outpath="/home/sferrone/plots/"+version[k]+"/"+model[k]+"/individual/"
        os.makedirs(outpath,exist_ok=True)
        outpath="/home/sferrone/plots/"+version[k]+"/"+model[k]+"/individual/"+GCnames[k] +"/"
        os.makedirs(outpath,exist_ok=True)        
        pool.apply_async(main,args=(GCnames[k],version[k],model[k]))
    # main(GCnames[k],version[k],model[k])
    
    pool.close()
    pool.join()
    pool.terminate()
    print(time.time()-st, "s")