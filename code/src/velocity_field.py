import matplotlib.pyplot as plt 
import numpy as np 
import trace_orbit as TORB
import orbitCoords as OC
import os
import multiprocessing as mp
import time





def loadOrbitAndStream(GCname,version,model,dt=0.2):
    t,W,Xp,Yp,Zp,COM,stream=TORB.load_orbit_stream_COM(GCname,version=version,model=model)
    VXp,VYp,VZp=stream['galactocentric']['VX'][:],stream['galactocentric']['VY'][:],stream['galactocentric']['VZ'][:]
    cond = np.abs(t) < dt
    ts,Ws = t[cond],W[cond,:]
    past,future=(ts < 0),(ts > 0)
    x,y,z = Ws[:,0],Ws[:,1],Ws[:,2]
    vx,vy,vz = 10*Ws[:,3],10*Ws[:,4],10*Ws[:,5]
    # put in km/s
    VXp*=10
    VYp*=10
    VZp*=10
    return ts,x,y,z,vx,vy,vz,Xp,Yp,Zp,VXp,VYp,VZp,stream,COM


def getOrbitalCoords(x,y,z,vx,vy,vz,Xp,Yp,Zp,VXp,VYp,VZp,ts,xcom,ycom,zcom):
    # initialize 
    xprime,indexes,tt=np.zeros(Xp.shape),np.zeros(Xp.shape),np.zeros(Xp.shape)
    star_coords,velCoords=np.zeros((Xp.shape[0],3)), np.zeros((Xp.shape[0],3))
    xprimeORB=np.zeros(x.shape)
    tt=np.zeros(Xp.shape)
    xprimeORB=OC.define_orbital_coordinate(x, y, z, xcom, ycom, zcom, xprimeORB)
    tt, xprime, star_coords, indexes, velCoords = OC.getorbcoords(Xp, Yp, Zp, VXp, VYp, VZp, ts, x, y, z, vx, vy, vz,xprimeORB,tt,xprime,star_coords,velCoords,indexes)
    return tt,xprime,star_coords,indexes,velCoords,xprimeORB
        

def main(GCname,version,model,lacolour="tesc"):
    """ 
    CREATE THE VELOCITY FIELD PLOTS FOR ALL CLEAN STREAMS
    DO THIS WITHIN THE LOCAL COORDIANATE SYSTEM
    """
    plt.style.use('dark_background')
    dt=0.3
    ts,x,y,z,vx,vy,vz,Xp,Yp,Zp,VXp,VYp,VZp,stream,COM=loadOrbitAndStream(GCname,version,model,dt=dt)

    xcom,ycom,zcom=COM.x.value,COM.y.value,COM.z.value



    # # initialize 
    # xprime,indexes,tt=np.zeros(Xp.shape),np.zeros(Xp.shape),np.zeros(Xp.shape)
    # star_coords,velCoords=np.zeros((Xp.shape[0],3)), np.zeros((Xp.shape[0],3))
    # xprimeORB=np.zeros(x.shape)
    # xprimeORB=np.zeros(x.shape)
    # tt=np.zeros(Xp.shape)
    # xprimeORB=OC.define_orbital_coordinate(x, y, z, xcom, ycom, zcom, xprimeORB)
    # tt, xprime, star_coords, indexes, velCoords = OC.getorbcoords(Xp, Yp, Zp, VXp, VYp, VZp, ts, x, y, z, vx, vy, vz,xprimeORB,tt,xprime,star_coords,velCoords,indexes)
    # cond0 = stream['energy']['Tescape'][:]>0
    # cond1 = xprime < 0
    # cond2 = xprime > 0
    # cond1 = np.logical_and(cond0,cond1)
    # cond2 = np.logical_and(cond0,cond2)
    
    tt,xprime,star_coords,indexes,velCoords,xprimeORB = \
        getOrbitalCoords(x,y,z,vx,vy,vz,Xp,Yp,Zp,VXp,VYp,VZp,ts,xcom,ycom,zcom)
    cond0 = stream['energy']['Tescape'][:]>0
    cond1 = xprime < 0
    cond2 = xprime > 0
    cond1 = np.logical_and(cond0,cond1)
    cond2 = np.logical_and(cond0,cond2)    
    XX,YY=xprime[cond0],star_coords[cond0,0]

    my_escapetime = stream['energy']['Tescape'][cond0]
    my_escapetime = 5-(my_escapetime/10) 
    ## how to convert properly?
    mycolor=np.ones((XX.shape[0],4))
    mycolor[:,3]=0.4

    NX,NY=100,100
    XXX,YYY = np.meshgrid(np.linspace(XX.min(),XX.max(),NX),np.linspace(YY.min(),YY.max(),NY))
    VVXX,xedges,yedges=np.histogram2d(XX,YY,bins=[XXX[0,:],YYY[:,0]],weights=velCoords[cond0,0])
    VVYY,xedges,yedges=np.histogram2d(XX,YY,bins=[XXX[0,:],YYY[:,0]],weights=velCoords[cond0,1])
    TTESC,xedges,yedges=np.histogram2d(XX,YY,bins=[XXX[0,:],YYY[:,0]],weights=my_escapetime,)
    counts,xedges,yedges=np.histogram2d(XX,YY,bins=[XXX[0,:],YYY[:,0]])
    # print("OKOKOKOK")
    VVXX/=counts
    VVYY/=counts
    TTESC/=counts
    # print("NOT OKOKOKOK")

    NORM = np.sqrt(VVXX**2 + VVYY**2)

    xcenters = [(xedges[i]+xedges[i+1])/2 for i in range(len(xedges)-1)]
    ycenters = [(yedges[i]+yedges[i+1])/2 for i in range(len(yedges)-1)]
    XCEN,YCEN = np.zeros(VVXX.shape),np.zeros(VVYY.shape)
    for i in range(len(xcenters)-1):
        XCEN[i,:]=xcenters[i]
    for i in range(len(ycenters)-1):
        YCEN[:,i]=ycenters[i]
    quiveropts = dict(color=mycolor,headlength=15,headwidth=15,pivot='middle',scale=100,cmap=plt.cm.get_cmap("rainbow"))

    fig,ax=plt.subplots(1,1,figsize=(15,5))
    if lacolour=="tesc":
        quiv=ax.quiver(XCEN,YCEN,VVXX/NORM,VVYY/NORM,TTESC,**quiveropts)
    else:
        quiv=ax.quiver(XCEN,YCEN,VVXX/NORM,VVYY/NORM,NORM,**quiveropts)
    ax.set_ylabel("y' (kpc)")
    ax.set_xlabel("x' (kpc)")
    # ax.set_aspect("equal")
    ax.grid("ON",alpha=0.2)
    ourdir="/home/sferrone/plots/"+version+"/"+model+"/individual/"+GCname+"/"
    ax.set_title(GCname)
    cbar=fig.colorbar(quiv)
    if lacolour=="tesc":
        cbar.set_label(r"$t_{esc}$ (Gyr)",rotation=270)
    else:
        cbar.set_label(r"$\delta v$ (km/s)",rotation=270)
    fig.tight_layout()
    print("SAVING!!!")
    print(ourdir+"velocity-field.png")
    fig.savefig(ourdir+"velocity-field-tesc.png",dpi=400)    
    plt.close(fig)    
    
    
if __name__=="__main__":
    
    model="PII"
    version="EDR3"
    home="/home/sferrone/"
    path="/home/sferrone/plots/"
    os.makedirs(path,exist_ok=True)
    path="/home/sferrone/plots/"+version+"/"
    os.makedirs(path,exist_ok=True)
    path="/home/sferrone/plots/"+version+"/model/"
    os.makedirs(path,exist_ok=True)
    path="/home/sferrone/plots/"+version+"/"+model+"/"
    os.makedirs(path,exist_ok=True)
    path="/home/sferrone/plots/"+version+"/"+model+"/individual/"
    os.makedirs(path,exist_ok=True)
    
    pool=mp.Pool(mp.cpu_count())
    print("POK PK PK")
    GCnames,version,model=np.loadtxt("../little-data/clean_stream.txt",delimiter=',',dtype=str,unpack=True)
    st=time.time()
    for i in range(len(GCnames)):
        print()
        os.makedirs("/home/sferrone/plots/"+version[i]+"/"+model[i]+"/individual/"+GCnames[i]+"/", exist_ok=True)
        pool.apply_async(main,args=(GCnames[i],version[i],model[i]))
    i=0
    # main(GCnames[i],version[i],model[i])
    pool.close()
    pool.join()
    pool.terminate() 

    
    print("elapsed time:" , time.time()-st, "s")