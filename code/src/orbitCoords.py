import numpy as np 
from astropy.io import fits
import astropy.coordinates as coord
import astropy.units as u
import sys 
sys.path.append("/home/sferrone/GC-tidal-loss/simulations/analytic-GC-tidal-loss/code")
import inputMW
from numba import njit, prange
import numba

# the functions for putting the coordiantes relative to the orbit

def getCOM(GCname,GCsfile="/home/sferrone/GC-tidal-loss/simulations/analytic-GC-tidal-loss/clusterDATA/2022-01-14-kinematics-Baumgardt.fits"):
    me=fits.open(GCsfile)
    gc_frame = inputMW.ref_frame()
    GCIDX,=np.where(me[1].data['ID']==GCname)[0]
    RA=me[1].data['RA'][GCIDX]
    DEC=me[1].data['DEC'][GCIDX]
    D=me[1].data['Rsun'][GCIDX]
    pm_ra_cosdec=me[1].data["mualpha"][GCIDX]
    pm_dec=me[1].data["mu_delta"][GCIDX]
    RV=me[1].data["RV"][GCIDX]

    c1 = coord.SkyCoord(ra=RA*u.degree,dec=DEC*u.degree,distance=D*u.kpc,\
        pm_ra_cosdec=pm_ra_cosdec*u.mas/u.yr,pm_dec=pm_dec*u.mas/u.yr,\
        radial_velocity=RV*u.km/u.s)
    COM=c1.transform_to(gc_frame)    
    return COM



def define_orbital_coordinate(x, y, z, xcom, ycom, zcom, pcoord):
    """
    Distance from the center of mass
    assumes that later indicies of xyz happen later in time
    pcoord is path coordinate
    assumes pcoord is initialized as
    pcoord = np.zeros(x.shape)
    """
    com_index = np.argmin((x - xcom) ** 2 + (y - ycom) ** 2 + (z - zcom) ** 2)
    dx = np.diff(x)
    dy = np.diff(y)
    dz = np.diff(z)
    dr = np.sqrt(dx ** 2 + dy ** 2 + dz ** 2)
    ahead = dr[com_index::]
    behind = dr[:com_index]

    pcoord[com_index+1::] = np.cumsum(ahead)
    pcoord[:com_index] = -np.flip(np.cumsum(np.flip(behind)))
    return pcoord




# @njit(parallel=True)
def getorbcoords(Xp, Yp, Zp, VXp, VYp, VZp, t, x, y, z, vx, vy, vz, pcoord, tt, xprime, orbCoords, velCoords, indexes):
    """
    ARGUMENTS:
        XP,YP,ZP = particles
        t,x,y,z,vx,vy,vx = orbit of the GC
        pcoord = x' of orbit 
        tt, xprime, indexes : should be initialized to np.zeros(Xp.shape)
        orbCoords, velCoords: should be initalized as np.shape((Xp.shape[0],3))
    RETURNS:
        star_coords : the position of each particle relaitve to the orbit 
        xprime: distance of each particle along the orbit to the COM of the parent
        tt: the time when the GC was at the current closest position of the orbit to the star
        indexes: orbit indicies corresponding to the particle output
    compensate the off set
    """
    for i in range(Xp.shape[0]):
        dx,dy,dz = Xp[i]-x,Yp[i]-y,Zp[i]-z
        dist=dx**2 + dy**2 + dz**2
        indx=np.argmin(dist) 
        P = np.array([Xp[i],Yp[i],Zp[i]])
        A = np.array([x[indx],y[indx],z[indx]])
        dX = P-A
        V = np.array([vx[indx],vy[indx],vz[indx]])
        VP = np.array([VXp[i], VYp[i], VZp[i]])
        VP=VP-V
        vunit =V/np.linalg.norm(V)
        # adjust the point slightly
        compV=np.dot(dX,vunit)*vunit
        nearP=A+compV # near point
        runit = nearP/np.linalg.norm(nearP)
        zunit=np.cross(runit,vunit)
        rcomp=np.dot(dX,runit)
        zcomp=np.dot(dX,zunit)
        vcomp=np.dot(dX,vunit)
        
        vxout = np.dot(VP,vunit)
        vyout = np.dot(VP,runit)
        vzout = np.dot(VP,zunit)
        
        # outputs
        tt[i] = t[indx]
        xprime[i]=pcoord[indx]
        orbCoords[i,:] = np.array([rcomp,zcomp,vcomp])
        velCoords[i,:] = np.array([vxout,vyout,vzout])
        indexes[i]=indx
        
    return  tt, xprime, orbCoords, indexes,velCoords
