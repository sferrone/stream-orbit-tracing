import orbitCoords
import matplotlib.pyplot as plt 
from mpl_toolkits.mplot3d import Axes3D
from scipy.linalg import lstsq
import numpy as np 
import h5py
import json
from scipy.io import FortranFile
import sys
import time
import trace_orbit as TORB
import os 
import multiprocessing as mp




def main(GCname="NGC3201",version="EDR3",model="PII"):
    t,W,Xp,Yp,Zp,COM,stream=TORB.load_orbit_stream_COM(GCname,version=version,model=model)
    dt=0.2
    past,future,x,y,z,vx,vy,vz=TORB.absTimeFilter(dt,t,W)
    
    A=np.ones((Xp.shape[0],6))
    A[:,1],A[:,2],A[:,3] = Xp**2, Yp**2, Xp*Yp
    A[:,4],A[:,5] = Xp, Yp    
    fit,resid,rnk,s=lstsq(A,Zp) 
    N=10
    def getminmax(mya,factor=1.2):
        if mya.min() < 0:
            xmin = mya.min()*1.2
        else:
            xmin = mya.min()/1.2

        if mya.max() > 0:
            xmax = mya.max()*1.2
        else:
            xmax = mya.min()/1.2
        return xmin,xmax

    xmin,xmax = getminmax(Xp)
    ymin,ymax = getminmax(Yp)
    zmin,zmax = getminmax(Zp)

    xs = np.linspace(xmin,xmax,N)
    ys = np.linspace(ymin,ymax,N)
    X,Y=np.meshgrid(xs,ys)
    Z = np.zeros(X.shape)

    for r in range(X.shape[0]):
        for c in range(X.shape[1]):
            Z[r,c] = fit[0] + fit[1]*X[r,c]**2 + fit[2]*Y[r,c]**2 + \
                fit[3]*X[r,c]*Y[r,c] + fit[4]*X[r,c] + fit[5]*Y[r,c]
                
            
            

    plt.style.use('dark_background')
    fig=plt.figure()
    ax = plt.subplot(111, projection='3d')
    ax.plot3D(x[past],y[past],z[past], c='r',linewidth=1)
    ax.plot3D(x[future],y[future],z[future], c='g',linewidth=1)
    ax.scatter(Xp,Yp,Zp,alpha=0.1,s=1)
    xlim = ax.get_xlim()
    ylim = ax.get_ylim()
    ax.plot_wireframe(X,Y,Z, color='k',rstride=1, cstride=1)
    fig.suptitle(GCname)
    ax.set_xlabel("X (kpc)")
    ax.set_ylabel("Y (kpc)")
    ax.set_zlabel("Z (kpc)")
    ax.set_xlim(xmin,xmax)
    ax.set_ylim(ymin,ymax)
    ax.set_zlim(zmin,zmax)
    os.makedirs("../../videos/",exist_ok=True)
    os.makedirs("../../videos/spin/",exist_ok=True)
    os.makedirs("../../videos/spin/"+GCname+"/",exist_ok=True)
    print("doing", GCname)
    c=0
    for j in range(0,360,3):
        print(GCname,j)
        ax.view_init(30,j)
        fig.savefig("../../videos/spin/"+GCname+"/"+GCname+"_plane_"+str(j).zfill(4)+".png",dpi=300)
    c+=j
    for j in range(0,360,3):
        theta=30+j
        theta=np.mod(theta,360)
        print(GCname,c)
        ax.view_init(theta,0)
        fig.savefig("../../videos/spin/"+GCname+"/"+GCname+"_plane_"+str(c).zfill(4)+".png",dpi=300)        
        c+=1
    plt.close(fig)

    
if __name__=="__main__":
    st = time.time()    
    pool=mp.Pool(mp.cpu_count())
    print("# CPUs:",mp.cpu_count())

    args=np.loadtxt("../little-data/clean_stream.txt",dtype=str,delimiter=',')

    for i in range(args.shape[0]-10):
        GCname,version,model=args[i,0],args[i,1],args[i,2]
        main(GCname,version,model)
        # pool.apply_async(main,args=(GCname,version,model))

    # pool.close()
    # pool.join()
    # pool.terminate() 
    print("elapsed time:" , time.time()-st, "s")    