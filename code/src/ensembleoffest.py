import matplotlib.pyplot as plt 
import numpy as np 
import h5py
import json 
import sys 
sys.path.append("/home/sferrone/GC-tidal-loss/experiments/functions/")
import inputGC
from galpy.potential import MWPotential2014 as mwp14
from galpy.potential import vcirc
import orbitCoords as OC
import trace_orbit as TO 
from matplotlib import cm
import os 
import pandas as pd




# get radius for each ? which ixxs not a circular orbit so i'll get the current radius?
def getRcutoff(msat,R,poten):
    """
    purpose
    get the radius at which the stream is cut off
    
    """
    G=6.67*10**-11
    a=(1/ (3.086 *10**(19)) )
    b=1/1000
    c=2*10**30
    myG=(G*a*(b**2)*c)
    myvcirc=220*vcirc(poten,R/8)
    
    term=(msat*myG/(3*myvcirc**2))**(1/3)
    return 2.88*term*R**(2/3)


def makeCompiledCSV(dataname):
    """
    purpose
        make a CSV with the 
    """
    # change global plot parameters
    # params = {'axes.labelsize': 20,
    #       'axes.titlesize': 16}
    # plt.rc('axis', labelsize=20)
    # plt.rcParams.update(params)

    # load the GC names with the correct version and model
    k=0
    GCnames,version,model=np.loadtxt("../little-data/clean_stream.txt",delimiter=',',dtype=str,unpack=True)
    f=open("../little-data/config.json")
    C = json.load(f) # the config contains the orbital path names
    dt=0.3
    backwardpath,forwardpath=TO.orbitpath(C)
    streampath=TO.get_streampath(version=version[k],model=model[k])
    # initialize output arrays
    masses,radii,trailY,leadY,R3DCOM=[],[],[],[],[]
    
    for k in range(len(GCnames)):
        print("calculating offset for", GCnames[k])
        stream=h5py.File(streampath+GCnames[k]+".h5")
        # load COM
        COM = OC.getCOM(GCnames[k])
        mass,radius=inputGC.Plummer(GCnames[k]) 
        masses.append(mass)
        radii.append(radius)
        # load ORBIT
        t,W=TO.load_orbit(backwardpath,forwardpath,GCnames[k],C)
        cond = np.abs(t) < dt
        ts,Ws = t[cond],W[cond,:]
        x,y,z = Ws[:,0],Ws[:,1],Ws[:,2]
        vx,vy,vz = Ws[:,3],Ws[:,4],Ws[:,5]
        # initialize particle arrays in galactic coordinates
        Xp,Yp,Zp=np.array(stream['galactocentric']['X']),np.array(stream['galactocentric']['Y']),np.array(stream['galactocentric']['Z'])
        VXp, VYp, VZp = np.array(stream['galactocentric']['VX']),np.array(stream['galactocentric']['VY']),np.array(stream['galactocentric']['VZ'])
        # initalize the outputs in along-orbit coordinates
        xprime,orbCoords,indexes,tt=np.zeros(Xp.shape),np.zeros((Xp.shape[0],3)),np.zeros(Xp.shape),np.zeros(Xp.shape)
        velCoords=np.zeros((Xp.shape[0],3))
        xprimeORB=np.zeros(x.shape)
        xcom,ycom,zcom=COM.x.value,COM.y.value,COM.z.value
        R3DCOM.append( np.sqrt(xcom**2 + ycom**2 + zcom**2 ))
        xprimeORB=np.zeros(x.shape)
        tt=np.zeros(Xp.shape)
        # get the orbital coordaintes
        xprimeORB=OC.define_orbital_coordinate(x, y, z, xcom, ycom, zcom, xprimeORB)
        tt, xprime, orbCoords, indexes,velCoords = OC.getorbcoords(Xp, Yp, Zp, VXp, VYp, VZp, t, x, y, z, vx, vy, vz, xprimeORB, tt, xprime, orbCoords, velCoords, indexes)
        cond0=stream['energy']['Tescape'][:]>0
        cond1 = xprime < 0
        cond2 = xprime > 0
        cond1 = np.logical_and(cond0,cond1)
        cond2 = np.logical_and(cond0,cond2)
        trailY.append(np.mean(orbCoords[cond1,0]))
        leadY.append(np.mean(orbCoords[cond2,0]))    
    Rcutoff = []
    for k in range(len(GCnames)):
        Rcutoff.append(getRcutoff(masses[k],R3DCOM[k],mwp14))

    colnames = "GCnames,trailing,leading,masses,rCOMGal,RGC,Rjacobii"
    outputpath="../outdata/"
    os.makedirs(outputpath,exist_ok=True)
    # save data as a pandas csv
    df=pd.DataFrame({"GCnames":GCnames,"trailing":trailY,"leading":leadY,"masses":masses,"rCOMGal":R3DCOM,"RGC":radii,"Rjacobii":Rcutoff})
    df.to_csv(outputpath+dataname,index=False)
    return GCnames,leadY,trailY,masses,R3DCOM,radius,Rcutoff


def main():
    """
    PURPOSE
    COMPARE THE delta y' FOR EACH STREAM TO THE 
    THEORETICAL JACOBBI RADIUS FOR A GC ON AN ORBIT 
    IN AN EQUICALENT SPHERICAL POTENTIAL
    """

    dataname="offsetStats.csv"
    datapath="../outdata/"
    # check if the file exists
    if os.path.isfile(datapath+dataname):
        print("file exists")
        GCnames,leading,trailing,masses,RfromGal,RGC,radiijacobii=np.loadtxt(datapath+dataname,delimiter=',',unpack=True,dtype=str,skiprows=1)
        radiijacobii=np.array(radiijacobii,dtype=float)
        RGC=np.array(RGC,dtype=float)
        RfromGal=np.array(RfromGal,dtype=float)
        masses=np.array(masses,dtype=float)
        trailing=np.array(trailing,dtype=float)
        leading=np.array(leading,dtype=float)
    else:
        print("file does not exist")
        GCnames,leading,trailing,masses,RfromGal,RGC,radiijacobii=makeCompiledCSV(dataname)

    plt.style.use('dark_background')
    outputpath="/home/sferrone/plots/"
    GCnames,version,model=np.loadtxt("../little-data/clean_stream.txt",delimiter=',',dtype=str,unpack=True)

    k=0
    os.makedirs(outputpath,exist_ok=True)
    os.makedirs(outputpath+version[k]+"/",exist_ok=True)
    os.makedirs(outputpath+version[k]+"/"+model[k]+"/",exist_ok=True)
    os.makedirs(outputpath+version[k]+"/"+model[k]+"/ensemble",exist_ok=True)
    finaloutpath=outputpath+version[k]+"/"+model[k]+"/ensemble/"
    mycmap=cm.get_cmap('hsv')
    mycolors=mycmap(np.linspace(0,1,len(GCnames)))

    # for randomly labeling the points
    np.random.seed(0)
    updown=np.random.rand(len(GCnames))
    updown[updown>0.5] = True
    updown[updown<0.5] = False

    fig,ax=plt.subplots(1,1,figsize=(9,6))
    ax.scatter(RGC,trailing,c=mycolors)
    ax.scatter(RGC,leading,c=mycolors)
    ax.set_ylabel(r"$\bar{\delta y '}$ (kpc)")
    ax.set_xlabel("Characteristic radius (kpc)")
    ax.grid(True, alpha=0.1)
    for i in range(len(updown)):
        if updown[i]:
            ax.text(RGC[i],leading[i],GCnames[i])
        else:
            ax.text(RGC[i],trailing[i],GCnames[i])
    fig.tight_layout()
    print('saving figure to ',finaloutpath+"yprime_RC.png")
    fig.savefig(finaloutpath+"yprime_RC.png")
    plt.close(fig)


    fig,ax=plt.subplots(1,2,figsize=(16,7))
    ax[0].scatter(masses,trailing,c=mycolors)
    ax[0].scatter(masses,leading,c=mycolors)
    ax[0].set_xlabel(r"$M_{\odot}$ ")
    ax[0].set_ylabel(r"$\bar{\delta y '}$ (kpc)")
    ax[0].grid(True,alpha=0.2)

    ax[1].scatter(masses,trailing,c=mycolors)
    ax[1].scatter(masses,leading,c=mycolors)
    ax[1].set_xlabel(r"$M_{\odot}$ ")
    ax[1].set_ylabel(r"$\bar{\delta y '}$ (kpc)")
    ax[1].set_xscale('log')
    ax[1].grid(True,alpha=0.2)    

    for i in range(len(updown)):
        if updown[i]:
            ax[0].text(masses[i],leading[i],GCnames[i])
            ax[1].text(masses[i],leading[i],GCnames[i])
        else:
            ax[0].text(masses[i],trailing[i],GCnames[i])
            ax[1].text(masses[i],trailing[i],GCnames[i])

    fig.tight_layout()
    fig.savefig(finaloutpath+"yprime_MASS.png",dpi=200)
    plt.close(fig)



    fig,ax=plt.subplots(1,1,figsize=(9,6))
    ax.scatter(radiijacobii,trailing,c=mycolors)
    ax.scatter(radiijacobii,leading,c=mycolors)
    ax.set_xlabel('jacobi radii (kpc)')
    ax.set_ylabel(r"$\bar{\delta y '}$ (kpc)")
    for i in range(len(updown)):
        if updown[i]:
            ax.text(radiijacobii[i],leading[i],GCnames[i])
        else:
            ax.text(radiijacobii[i],trailing[i],GCnames[i])

    ax.grid(True,alpha=0.2)
    fig.tight_layout()
    fig.savefig(finaloutpath+'yprime_jacobiRadii.png')
    plt.close(fig)

    # plot the yprime against the mass/rGC
    fig,ax=plt.subplots(1,2,figsize=(16,7))
    ax[0].scatter(masses/RGC,trailing,c=mycolors)
    ax[0].scatter(masses/RGC,leading,c=mycolors)
    ax[0].set_xlabel(r"$M_{\odot}/R_{GC}$ ")
    ax[0].set_ylabel(r"$\bar{\delta y '}$ ")
    # same but log x
    ax[1].scatter(masses/RGC,trailing,c=mycolors)
    ax[1].scatter(masses/RGC,leading,c=mycolors)
    ax[1].set_xlabel(r"log$_{10} (M_{\odot}/R_{GC})$ $(M_{\odot}/$kpc)")
    ax[1].set_ylabel(r"$\bar{\delta y '}$ (kpc)")
    ax[1].set_xscale('log')
    for i in range(len(updown)):
        if updown[i]:
            ax[0].text(masses[i]/RGC[i],leading[i],GCnames[i])
            ax[1].text(masses[i]/RGC[i],leading[i],GCnames[i])
        else:
            ax[0].text(masses[i]/RGC[i],trailing[i],GCnames[i])
            ax[1].text(masses[i]/RGC[i],trailing[i],GCnames[i])
    ax[0].grid(True,alpha=0.2)
    ax[1].grid(True,alpha=0.2)
    fig.tight_layout()
    fig.savefig(finaloutpath+'yprime_massOverRgc.png')
    plt.close(fig)
    

if __name__=="__main__":
    print("starting script")
    main()